import $ from 'jquery';
import Swiper from 'swiper';

$(document).ready(function () {
    //START: Header
    if (window.outerWidth <= 768) {
        $('#header').addClass('header-background--mobile')

        if ($('body').find('#welcome').length) {
            $('#header').css({position: 'absolute'})
        } else $('#header').css({backgroundColor: '#f4f7f7'})
    } else $('#header').removeClass('header-background--mobile')

    $(document).on('click', '#js-mobile-btn-header-menu', toggleOpenHeaderMenu);

    function toggleOpenHeaderMenu() {
        $('#js-mobile-btn-header-menu').toggleClass('hidden');

        $('#js-body-wrapper').toggleClass('active');
        $('#js-header-menu-mobile').toggleClass('opened');

        setTimeout(function () {
            $('#js-mobile-btn-header-menu').removeClass('hidden').toggleClass('header__btn-menu-mobile--opened');
        }, 500);

        if (window.outerWidth <= 400) {
            $('#js-header-section-left').addClass('hidden');

            setTimeout(function () {
                $('#js-header-section-left').removeClass('hidden');
            }, 500);
        } else $('#js-header-logo').toggleClass('lighten');
    }

    //Mobile menu
    $(document).on('click', '.header-mobile-menu__links-item', function () {
        $('.header-mobile-menu__links-item.header-mobile-menu__links-item--active').removeClass('header-mobile-menu__links-item--active');

        $(this).addClass('header-mobile-menu__links-item--active');

        toggleOpenHeaderMenu();
    });

    if (window.outerWidth <= 400) {
        $("#js-header-menu-mobile").scroll(function () {
            var $header_section_top = $('#js-header-section-left, #js-mobile-btn-header-menu');

            if ($(this).scrollTop() >= $header_section_top.height()) {
                $header_section_top.addClass('hidden');
            } else $header_section_top.removeClass('hidden');
        });
    }
    //Mobile menu
    //START: Header

    //START: Features
    var features_list_id = '#js-features-list',
        features_list = $(features_list_id).children(),
        features_list_item_length = features_list.length;

    if (window.outerWidth <= 768) {
        for (var i = 0; i < features_list_item_length; i++) {
            if (i > 3) {
                features_list.eq(i).addClass('hidden');
            }
        }
    }

    $(document).on('click', '#js-features-list-btn', function () {
        $(this).addClass('hidden');

        $('.features-list__item.hidden').removeClass('hidden');
    });
    //END: Features

    //START: Reviews
    if (window.outerWidth <= 1380) {
        var mySwiper = new Swiper('#js-reviews-slider', {
            slidesPerView: 'auto',
            spaceBetween: 40,
            freeMode: true,
            pagination: {
                el: '#js-reviews-slider-pagination',
                clickable: true,
            },
            breakpoints: {
                0: {
                    slidesOffsetAfter: 75,
                    spaceBetween: 43,
                },
                767: {
                    spaceBetween: 37,
                    slidesPerView: 2,
                },
            }
        });
    }
    //END: Reviews

    //START: Packages
    if (window.outerWidth >= 376 && window.outerWidth <= 1380) {
        var packagesSwiper = new Swiper('#js-packages-slider', {
            slidesPerView: 'auto',
            spaceBetween: 40,
            freeMode: true,
            pagination: {
                el: '#js-packages-slider-pagination',
                clickable: true,
            },
            breakpoints: {
                0: {
                    spaceBetween: 31,
                },
                767: {
                    slidesPerView: 2,
                }
            }
        });
    }
    //END: Packages
});
