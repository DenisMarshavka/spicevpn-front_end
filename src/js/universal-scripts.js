import $ from 'jquery';
// var Drop = require('./libraries/dropDown.min')

//START: Specifications
//Scroll events
var scroll_page_to_section_working = false;

$(document).on('scroll', 'body, html', function (event) {
    if (scroll_page_to_section_working) {
        event.preventDefault();
        event.stopPropagation();
    }
});
//Scroll events

//Drop-Downs with Languages
$('#js-drop-down-lang--mobile, #js-drop-down-lang--footer').each(function () {
    var heightSelectedLangItem = parseInt( $(this).find('.js-drop-down-item.js-drop-down-item--selected').css('height'));

    if ($(this).find('ul').length && $(this).find('ul').children().length && !isNaN(heightSelectedLangItem)) {
        var sumAllHeight = heightSelectedLangItem * ($(this).find('ul').children().length + 1);

        if ($(this).hasClass('js-drop-down--mobile')) {
            sumAllHeight -= 20
        } else sumAllHeight += 1

        $(this).find('ul').css({transform: 'translateY(-' + sumAllHeight + 'px)'})
    }
});
//Drop-Downs with Languages
//END: Specifications

$(document).ready(function () {
    //START: Specifications
    var id_name_header_menu                  = '#js-header-menu',
        approve_to_change_header_item_active = true;

    //Scroll to these sections, of the menu links
    if (window.location.hash) pageScrollTo(window.location.hash, true);

    $(document).on('click', '.header-menu__item, .navigation-item__link, .header-mobile-menu__links-item, .js-navigation-link', function (event) {
        var scroll_to_section = $(this).children('a').attr('href').split('/')[1] || '';

        if (scroll_to_section) {
            if (scroll_to_section.indexOf('#') > -1 && $(scroll_to_section).length) {
                approve_to_change_header_item_active = false;
                event.preventDefault();

                pageScrollTo(scroll_to_section, false, (!$(this).hasClass('js-navigation-link')) ? 1000 : 500);
            }
        }
    });

    function pageScrollTo(to_section, reset_offset_top, time_out) {
        var to_section = to_section || false;
        var reset_offset_top = reset_offset_top || false;
        var time_out = time_out || 1000;

        if (to_section) {
            if (to_section && $(to_section).length) {
                if (reset_offset_top) $('html, body').animate({scrollTop: 0}, 0);

                scroll_page_to_section_working = true;

                setTimeout(function () {
                    $('html, body').animate({scrollTop: parseInt($(to_section).offset().top) - ($('.header').height() * 2)}, 1000);

                    setTimeout(function () {
                        scroll_page_to_section_working = false;

                        approve_to_change_header_item_active = true;
                    }, 1000);
                }, time_out);
            } else if (!approve_to_change_header_item_active) approve_to_change_header_item_active = true;
        } else if (!approve_to_change_header_item_active) approve_to_change_header_item_active = true;
    }
    //Scroll to these sections, of the menu links

    //Header
    var offsets_sections                        = [],
        selectors_current_activated_header_item = '#js-header-menu li.header-menu__item--active',
        class_name_activated_header_item        = 'header-menu__item--active',
        $first_activated_header_item            = $(selectors_current_activated_header_item);

    $(document).on('click', id_name_header_menu + ' li', function () {
        $(selectors_current_activated_header_item).removeClass(class_name_activated_header_item);

        $(this).addClass(class_name_activated_header_item);
    });

    if (window.outerWidth >= 1200) {
        drawHeaderBackground();
        changeActiveHeaderItem();

        getSectionsOffset();

        $(document).scroll(function () {
            drawHeaderBackground();
            changeActiveHeaderItem();
        });
    }

    function drawHeaderBackground() {
        var $header = $('header');

        if ($(document).scrollTop() >= $header.height()) {
            $header.addClass('fixed');
        } else $header.removeClass('fixed');
    }

    function getSectionsOffset() {
        $('section[id]').each(function () {
            var current_section_id_name = $(this).attr('id'),
                current_section         = {
                    offset: $(this).offset().top - window.outerHeight / 7,
                    id: '#' + current_section_id_name
                 };

            offsets_sections.push(current_section);
        });
    }

    function changeActiveHeaderItem() {
        for (var section in offsets_sections) {
            if ($(document).scrollTop() > 0) {
                if (approve_to_change_header_item_active && $(document).scrollTop() >= offsets_sections[section].offset) {
                    if ($(id_name_header_menu + " li a[href='" + '/' + offsets_sections[section].id + "']").length) {
                        $(selectors_current_activated_header_item).removeClass(class_name_activated_header_item);

                        $(id_name_header_menu + " li a[href='" + '/' + offsets_sections[section].id + "']").parents('li').addClass(class_name_activated_header_item);
                    }
                }
            } else {
                $(selectors_current_activated_header_item).removeClass(class_name_activated_header_item);

                $first_activated_header_item.addClass(class_name_activated_header_item);
            }
        }
    }

    //START: Language DropDown
    $(document).on('click', '.js-drop-down-item--selected, .js-drop-down-item', function () {
        $(this).next().toggleClass('options-active')
        $(this).parents('.js-drop-down').toggleClass('active')

        if (!$(this).next().children('li').hasClass('js-drop-down-item')) $(this).next().children('li').addClass('js-drop-down-item')
        if (!$(this).hasClass('js-drop-down-item--selected') && !$(this).parents('ul').hasClass('options-active')) $(this).parents('.js-drop-down').removeClass('active')
    })
    //END: Language DropDown
    //Header

    //Order
    //START: Form validation
    function validateEmail(value) {
        var email = value || '';

        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    $('#js-order-input-email').on('keyup', function () {
        var isEmailValid = true

        if ($(this).val() && $.trim($(this).val())) isEmailValid = validateEmail($(this).val())

        if (!isEmailValid) {
            $(this).next().removeClass('hide')
        } else $(this).next().addClass('hide')
    })
    //END: Form validation
    //Order
    //END: Specifications

    //START: FAQ
    $(document).on('click', '.js-tabs .js-tabs-item button', function () {
        $(this).parent('li').toggleClass('tabs__item--active');
    });
    //END: FAQ

    //START: Packages
    const id_name_packages_slider         = '#js-packages-slider',
          active_packages_item_class_name = 'packages-slider__item--active';

    changePackageActive();
    changePackageSelectActive();

    $(document).on('click', id_name_packages_slider + " .packages-slider__item", function () {
        var current_package_name = $(this).find('.packages-slider__item-terms-title').text();

        setPackageActive(current_package_name, $(this));

        setPackageActive(current_package_name);
        changePackageSelectActive(current_package_name);
    });

    function setPackageActive(package_title, $_this) {
        var package_title = package_title || '';
        var $_this = $_this || false;
        const current_package_name = $.trim(package_title);

        $(id_name_packages_slider + ' .' + active_packages_item_class_name).removeClass(active_packages_item_class_name);

        if (current_package_name) {
            localStorage.setItem('active_package', JSON.stringify({'name': current_package_name}));

            if (!$_this) {
                $(id_name_packages_slider + " .packages-slider__item:contains(" + current_package_name +")").addClass(active_packages_item_class_name);
            } else $_this.addClass(active_packages_item_class_name);
        }
    }

    function changePackageActive() {
        var current_package_active_name;

        if ($('body').find(id_name_packages_slider).length) {
            try {
                current_package_active_name = JSON.parse( localStorage.getItem('active_package') ).name;
            } catch (e) {
                current_package_active_name = '';
            }

            if (current_package_active_name) {
                setPackageActive(current_package_active_name);
                changePackageSelectActive(current_package_active_name);
            }
        }
    }

    function changePackageSelectActive(value_option) {
        var activated_package_name;
        var value_option = value_option || false;

        if ($('body').find('#js-form-order-package-select').length) {
            if (value_option) {
                activated_package_name = value_option;
            } else if ($(id_name_packages_slider + ' .' + active_packages_item_class_name + ' .packages-slider__item-terms-title').length) {
                activated_package_name = $.trim( $(id_name_packages_slider + ' .' + active_packages_item_class_name + ' .packages-slider__item-terms-title').text() );
            }

            $('#js-form-order-package-select').val(activated_package_name);
        }
    }

    setTimeout(function () {
        $('#js-packages-slider.hidden').removeClass('hidden');
    }, 500);
    //END: Packages

    //START: Order
    $(document).on('click', '#js-form-order-open-coupon-btn', function () {
        $(this).addClass('hide');

        $('#js-form-order-coupon').addClass('show');
    });
    //START: Order
});
