var id_catalog_form_header_select        = '#js-header-form-select';
var id_catalog_form_header_select_mobile = '#js-header-form-select-mobile';
var id_catalog_form_footer_select        = '#js-footer-form-select';
var class_elements_select                = '.js-select';
var class_select_label                   = '.selectLabel';
var tag_name_option                      = 'option';
var class_name_selected_option           = 'options';
var class_selected_option                = '.' + class_name_selected_option;
var class_name_selected_option_active    = class_name_selected_option + '-active';

var defaultSelectBox = $(id_catalog_form_header_select + ' ' + class_elements_select);
var numOfOptions = defaultSelectBox.children(tag_name_option).length;

var footerSelectBox = $(id_catalog_form_footer_select + ' ' + class_elements_select);
var footerNumOfOptions = footerSelectBox.children(tag_name_option).length;

var headerMenuMobileSelect = $(id_catalog_form_header_select_mobile + ' ' + class_elements_select);
var headerMenuMobileNumOfOptions = headerMenuMobileSelect.children(tag_name_option).length;

// hide select tag
$(class_elements_select).addClass('s-hidden');

// wrapping default selectbox into custom select block
$(class_elements_select).wrap('<div class="cusSelBlock"></div>');

// creating custom select div
$(class_elements_select).after('<div class="selectLabel"></div>');

// getting default select box selected value
$(class_elements_select).each(function () {
    $(this).next().text( $(this).find(tag_name_option + '[selected]').text() ).attr('rel', $(this).find(tag_name_option + '[selected]').val());
});

// appending options to custom un-ordered list tag
//TODO: Make this script universal
var cusList = $('<ul/>', { 'class': 'options'} ).insertAfter($(id_catalog_form_header_select + ' ' + class_select_label));
var cusListFooterMenu = $('<ul/>', { 'class': 'options'} ).insertAfter($(id_catalog_form_footer_select + ' ' + class_select_label));
var cusListHeaderMenuMobile = $('<ul/>', { 'class': 'options'} ).insertAfter($(id_catalog_form_header_select_mobile + ' ' + class_select_label));

// generating custom list items
for(var i = 0; i < numOfOptions; i++) {
    $('<li/>', {
        text: defaultSelectBox.children(tag_name_option).eq(i).text(),
        rel: defaultSelectBox.children(tag_name_option).eq(i).val()
    }).appendTo(cusList);
}

for(var o = 0; o < footerNumOfOptions; o++) {
    $('<li/>', {
        text: footerSelectBox.children(tag_name_option).eq(o).text(),
        rel: footerSelectBox.children(tag_name_option).eq(o).val()
    }).appendTo(cusListFooterMenu);
}

for(var r = 0; r < headerMenuMobileNumOfOptions; r++) {
    $('<li/>', {
        text: headerMenuMobileSelect.children(tag_name_option).eq(r).text(),
        rel: headerMenuMobileSelect.children(tag_name_option).eq(r).val()
    }).appendTo(cusListHeaderMenuMobile);
}
//TODO: Make this script universal

function openList($this) {
    var $this = $this || false;

    if ($this) $this.next().addClass(class_name_selected_option_active);
}

function closeList($this, is_select_wrapper) {
    var $this = $this || false;
    var is_select_wrapper = is_select_wrapper || false;

    if ($this && !is_select_wrapper) {
        $this.parent().removeClass(class_name_selected_option_active);
    } else {
        if (!is_select_wrapper) {
            $('.' + class_name_selected_option).removeClass(class_name_selected_option_active);
        } else $this.next().removeClass(class_name_selected_option_active);
    }
}

// click event functions
$(class_select_label).click(function () {
    $(this).toggleClass('active');
    if( $(this).hasClass('active') ) {
        openList($(this));
        focusItems();
    } else {
        closeList($(this), true);
    }
});

$("." + class_name_selected_option + " li").on('keypress click', function(e) {
    var option_text = $(this).text();
    e.preventDefault();

    // $('.options li').siblings().removeClass();
    $(this).parent().children().siblings().removeClass();
    closeList($(this));

    $(this).parent().prev().removeClass('active').text(option_text).attr('rel', $(this).attr('rel'));

    // defaultSelectBox.val($(this).text());
    $(this).parents('.cusSelBlock').children(class_elements_select).val(option_text);

    $('.selected-item p span').text($(class_select_label).text());
});


function focusItems() {
    $(class_selected_option).on('focus', 'li', function() {
        var $this = $(this);
        $this.addClass('active').siblings().removeClass();
    }).on('keydown', 'li', function(e) {
        var $this = $(this);
        if (e.keyCode == 40) {
            $this.next().focus();
            return false;
        } else if (e.keyCode == 38) {
            $this.prev().focus();
            return false;
        }
    }).find('li').first().focus();
}

//My script
changeOptionActive();

$(document).on('click', '.options li', function () {
    changeOptionActive($(this));
});

function changeOptionActive($this) {
    var $this = $this || false;

    if ($this) {
        $this.parent().children().removeClass('active');

        $this.addClass('active');
    } else $('.options li' + ':first-of-type').addClass('active');
}

$(document).on('dblclick', '*', function () {
    closeList();

    $(class_select_label).removeClass('active');
});
