import $ from 'jquery';

$(document).ready(function () {
    //START: Specifications
    //Open/Close Pop-Up
    function changePopup(is_open) {
        var is_opening = is_open || false;
        $('html, body').css((is_opening) ? {overflowY: 'hidden', position: (window.outerWidth <= 568) ? 'fixed' : 'relative'} :{overflowY: 'auto', overflowX: 'hidden', position: 'relative'});
    }
    //Open/Close Pop-Up
    //END: Specifications

    //START: Header
    $(document).on('click', '#js-mobile-btn-header-menu, .header-mobile-menu__links-item', function () {
        changePopup( (!$(this).hasClass('header-mobile-menu__links-item')) ? !$(this).hasClass('header__btn-menu-mobile--opened') : $(this).hasClass('header__btn-menu-mobile--opened'));
    });
    //END: Header
});
