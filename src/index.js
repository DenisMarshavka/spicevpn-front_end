// import AppService from './common/app.service';
// import {config} from './common/config';
import './modules/header.conponent'

//LIBRARIES CSS
// import './assets/css/libraries/slick.css';
import './assets/css/libraries/swiper.min.css';
// import './assets/css/libraries/dropDown.min.css'
// import 'owl.carousel/dist/assets/owl.carousel.css';
// import './assets/css/libraries/ion.rangeSlider.min.css';


//LIBRARIES JS
// import './js/libraries/slick.min';
import './js/libraries/swiper.min';
// import './js/libraries/dropDown.min'
// import './js/libraries/my-scripts/jquery.Select';
import './js/libraries/my-scripts/jquery.Select';
// import './js/libraries/fontawesome-all.min';

//FONTS
// import './assets/css/fontawesome-all.min.css';
// import './assets/css/material-design-iconic-font.css';

//SCRIPTS
import './js/universal-scripts';
import './js/common';
import './js/mobile-scripts';

//STYLES
import './assets/css/main.css';
import './assets/sass/main.sass';


